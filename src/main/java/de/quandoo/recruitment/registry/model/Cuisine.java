package de.quandoo.recruitment.registry.model;

public class Cuisine {

    private final String origin;

    public Cuisine(final String origin) {
        this.origin = origin;
    }

    public String getOrigin() {
        return origin;
    }

}
