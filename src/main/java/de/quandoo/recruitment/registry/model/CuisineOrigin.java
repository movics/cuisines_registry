package de.quandoo.recruitment.registry.model;

import java.util.HashMap;
import java.util.Map;

public enum CuisineOrigin {
  ITALIAN("italian"),
  FRENCH("french"),
  GERMAN("german");

  private String lang;

  CuisineOrigin(String lang) {
    this.lang = lang;
    Holder.LANG_MAP.put(lang, this);
  }

  public String getLang() {
    return lang;
  }

  private static class Holder {
    final static Map<String, CuisineOrigin> LANG_MAP = new HashMap<>();
  }

  public static CuisineOrigin getLanguage(String lang) {
    final CuisineOrigin language = Holder.LANG_MAP.get(lang);
    if (language == null) {
      throw new IllegalArgumentException("Invalid language requested: " + lang);
    }
    return language;
  }
}
