package de.quandoo.recruitment.registry.api.impl;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineOrigin;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

import static de.quandoo.recruitment.registry.model.CuisineOrigin.*;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

  private final EnumMap<CuisineOrigin, List<Customer>> inMemoryCuisineStorage;

  public InMemoryCuisinesRegistry() {
    inMemoryCuisineStorage = new EnumMap<>(CuisineOrigin.class);
    inMemoryCuisineStorage.put(ITALIAN, new LinkedList<>());
    inMemoryCuisineStorage.put(FRENCH, new LinkedList<>());
    inMemoryCuisineStorage.put(GERMAN, new LinkedList<>());
  }

  @Override
  public void register(final Customer userId, final Cuisine cuisine) {
    final CuisineOrigin cuisineName = getLanguage(cuisine.getOrigin());

    switch (cuisineName) {
      case ITALIAN:
        inMemoryCuisineStorage.get(ITALIAN).add(userId);
        break;
      case FRENCH:
        inMemoryCuisineStorage.get(FRENCH).add(userId);
        break;
      case GERMAN:
        inMemoryCuisineStorage.get(GERMAN).add(userId);
        break;
      default:
        System.err.println("Unknown cuisine, please reach johny@bookthattable.de to update the code");
        break;
    }
  }

  @Override
  public List<Customer> cuisineCustomers(final Cuisine cuisine) {
    if (cuisine == null) {
      return null;
    }

    final CuisineOrigin cuisineName = getLanguage(cuisine.getOrigin());

    switch (cuisineName) {
      case ITALIAN:
        return inMemoryCuisineStorage.get(ITALIAN);
      case FRENCH:
        return inMemoryCuisineStorage.get(FRENCH);
      case GERMAN:
        return inMemoryCuisineStorage.get(GERMAN);
      default:
        return null;
    }
  }

  @Override
  public List<Cuisine> customerCuisines(final Customer customer) {
    final List<Cuisine> customerCuisines = new ArrayList<>();

    if (inMemoryCuisineStorage.get(ITALIAN).contains(customer)) {
      customerCuisines.add(new Cuisine(ITALIAN.getLang()));
    }
    if (inMemoryCuisineStorage.get(FRENCH).contains(customer)) {
      customerCuisines.add(new Cuisine(FRENCH.getLang()));
    }
    if (inMemoryCuisineStorage.get(GERMAN).contains(customer)) {
      customerCuisines.add(new Cuisine(GERMAN.getLang()));
    }
    return customerCuisines;
  }

  @Override
  public List<Cuisine> topCuisines(final int limit) {
    final List<Cuisine> topCuisines = new ArrayList<>();

    int index = 0;
    for (CuisineOrigin cuisineOrigin : getCuisineOriginByPopularity()) {
      if (index == limit) {
        break;
      }

      topCuisines.add(new Cuisine(cuisineOrigin.getLang()));
      index++;
    }

    return topCuisines;
  }

  private List<CuisineOrigin> getCuisineOriginByPopularity() {
    return inMemoryCuisineStorage
            .entrySet()
            .stream()
            .sorted((entry1, entry2) -> {
              final int size1 = entry1.getValue().size();
              final int size2 = entry2.getValue().size();

              if (size1 == size2) {
                return 0;
              }
              return (size1 > size2) ? -1 : 1;
            })
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());
  }
}
