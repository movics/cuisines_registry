package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.impl.InMemoryCuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.List;

import static de.quandoo.recruitment.registry.model.CuisineOrigin.FRENCH;
import static de.quandoo.recruitment.registry.model.CuisineOrigin.ITALIAN;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class InMemoryCuisinesRegistryTest {

  private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

  @Test
  public void testBasicWorkflow() {
    cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
    cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
    cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

    final List<Customer> cuisine = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));

    assertThat(cuisine.get(0).getUuid(), is("1"));
  }

  @Test
  public void testNullCuisine() {
    cuisinesRegistry.cuisineCustomers(null);
  }

  @Test
  public void testNullCustomer() {
    cuisinesRegistry.customerCuisines(null);
  }

  @Test
  public void testMultipleCuisinesPerCustomer() {
    final Customer customer = new Customer("1");
    cuisinesRegistry.register(customer, new Cuisine("french"));
    cuisinesRegistry.register(customer, new Cuisine("german"));

    final List<Cuisine> customerCuisines = cuisinesRegistry.customerCuisines(customer);

    assertThat(customerCuisines.size(), is(2));
  }

  @Test
  public void testTopCuisine() {
    cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
    cuisinesRegistry.register(new Customer("4"), new Cuisine("french"));
    cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
    cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
    cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));
    cuisinesRegistry.register(new Customer("6"), new Cuisine("italian"));

    List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(1);

    assertThat(topCuisines.size(), is(1));
    assertThat(topCuisines.get(0).getOrigin(), is(ITALIAN.getLang()));
  }

  @Test
  public void testTop2Cuisines() {
    cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
    cuisinesRegistry.register(new Customer("4"), new Cuisine("french"));
    cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
    cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
    cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));

    List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(2);

    assertThat(topCuisines.size(), is(2));
  }


}